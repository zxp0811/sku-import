package com.itheima.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.goods.pojo.Sku;
import org.apache.ibatis.annotations.Mapper;


public interface SkuMapper extends BaseMapper<Sku> {
}
