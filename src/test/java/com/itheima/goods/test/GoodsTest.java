package com.itheima.goods.test;

import com.alibaba.fastjson.JSON;
import com.itheima.goods.pojo.Sku;
import com.itheima.goods.service.SkuService;
import com.itheima.goods.service.impl.SkuServiceImpl;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;

import org.elasticsearch.common.xcontent.XContentType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GoodsTest {

    private RestHighLevelClient client;

    @Autowired
    private SkuService skuService;

    @Before
    public void init() {
        //创建Rest客户端
        client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("192.168.200.129", 9200, "http")
                )
        );
    }

    @After
    public void close() throws IOException {
        client.close();
    }

    /**
     * 创建索引
     * @throws IOException
     */
    @Test
    public void testCreateIndex() throws IOException {
        //1. 创建CreateIndexRequest对象，并指定索引库名称
        CreateIndexRequest request = new CreateIndexRequest("sku");
        //2. 指定settings配置(可以默认)
        //3. 指定mapping配置
        request.mapping(
                "{\n" +
                        "    \"properties\": {\n" +
                        "      \"id\": {\n" +
                        "        \"type\": \"keyword\"\n" +
                        "      },\n" +
                        "      \"name\": {\n" +
                        "        \"type\": \"text\",\n" +
                        "        \"analyzer\": \"ik_smart\"\n" +
                        "      },\n" +
                        "      \"price\": {\n" +
                        "        \"type\": \"text\"\n" +
                        "      },\n" +
                        "      \"num\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"index\" : false\n" +
                        "      },\n" +
                        "      \"image\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"index\" : false\n" +
                        "      },\n" +
                        "      \"images\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"index\" : false\n" +
                        "      },\n" +
                        "      \"createTime\": {\n" +
                        "        \"type\": \"date\",\n" +
                        "        \"index\" : false\n" +
                        "      },\n" +
                        "      \"updateTime\": {\n" +
                        "        \"type\": \"date\",\n" +
                        "        \"index\" : false\n" +
                        "      },\n" +
                        "      \"categoryId\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"index\" : false\n" +
                        "      },\n" +
                        "      \"brandName\": {\n" +
                        "        \"type\": \"keyword\"\n" +
                        "      }\n" +
                        "    }\n" +
                        "  }",
                //指定映射的内容的类型为json
                XContentType.JSON);

        //4. 发起请求，得到响应
        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);

        //打印结果
        System.out.println("response = " + response.isAcknowledged());
    }

    /**
     * 从数据库中取得数据放入索引库
     * @throws IOException
     */
    @Test
    public void testBulkAddDocumentListFromDB() throws IOException {
        //查询数据

        List<Sku> skus = skuService.list();

        BulkRequest bulkRequest = new BulkRequest();
        //批量插入数据
        for (Sku sku : skus) {
            bulkRequest.add(new IndexRequest("sku")
                    .id(sku.getId().toString())
                    .source(JSON.toJSONString(sku), XContentType.JSON)
            );
        }
        // 4.发起请求
        BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);

        System.out.println("status: " + bulkResponse.status());
    }
}
