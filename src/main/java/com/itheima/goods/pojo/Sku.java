package com.itheima.goods.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_sku")
public class Sku implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private String id;
    private String name;
    private int price;
    private int num;
    private String image;
    private String images;
    private Data createTime;
    private Data updateTime;
    private int categoryId;
    private String categoryName;
    private String brandName;
}

