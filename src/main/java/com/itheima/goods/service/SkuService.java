package com.itheima.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.goods.pojo.Sku;

public interface SkuService extends IService<Sku> {
}
